package com.nathanrittenhouse.usdtoyen;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CurrencyConverter extends AppCompatActivity {

    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    private static String json_url = "https://api.fixer.io/latest?base=USD";

    String json = "";
    String line = "";
    String rate = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_converter);
        editText01 = findViewById(R.id.editText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                System.out.println("\nTEST 1 ... Before AsynchExecution\n");
                BackgroundTask backgroundTask = new BackgroundTask();
                backgroundTask.execute();
                System.out.println("\nTEST 2 ... After AsynchExecution\n");
            }
        }
        );
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        // the method we use to say what happens before we start the A task
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // what happening during the A task
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        // what happens right after the A task
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

    @Override
    protected String doInBackground(Void... params) {
        try {
            URL web_url = new URL(CurrencyConverter.this.json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();
            httpURLConnection.setRequestMethod("GET");

            System.out.println("\nTESTING ... BEFORE connection method to URL\n");

            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            System.out.println("CONNECTION SUCCESSFUL\n");
            line = bufferedReader.readLine();

            while (line != null) {
                json += line;
                line = bufferedReader.readLine();
            }
            System.out.println("\nTHE JSON: " + json);

            // create new JSON object
            JSONObject obj_var = new JSONObject(json);
            JSONObject objRate = obj_var.getJSONObject("rates");

            rate = objRate.get("JPY").toString();
            System.out.println("\nWhat is rate: " + rate + "\n");

            // convert string to double
            final double rate_value = Double.parseDouble(rate);

            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsynchTask: " + rate_value);

            usd = editText01.getText().toString();

            // don't allow user to leave blank
            if (usd.equals("")) {
                textView01.setText("This field can't be blank.");
            } else {
                // convert string to double
                double dInputs = Double.parseDouble(usd);
                double result = dInputs * rate_value;

                // display the result
                textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));
                editText01.setText("");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("MYAPP", "unexpected JSON exception", e);
            System.exit(1);
        }
        return null;
        }
    }
}

